[Docker Images](https://gitlab.com/nest.lbl.gov/docker_images) Contains the files and documentation needed to manage the creation of Docker images that contain Nest software.

The following are the supported images:

*   `app-inspector` : Contains the files and documentation needed to create a Docker image that contains a set of utilities for file management. This is designed to be deployed along side other docker images that do not include these utiilties as they are not core to that image's purpose.

*   `nginx` : Contains the files and documentation needed to create a hardened `nginx` wen server, which supports SAML2 (using Shibboleth) authentication.

*   `spade` : Contains the files and documentation needed to create SPADE Docker images that can be used to deploy the vanilla version of the SPADE project.

*   `wildfly` : Contains the files and documentation needed to create a Wildfly Docker image that is custimized to support Nest deployments.
