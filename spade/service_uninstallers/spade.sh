#!/bin/bash
# Modified wildfly to undeploy the SPADE Service

echo "Removing SpadeDS from wildfly configuration if present"

java -jar /usr/share/java/saxon.jar \
        -s:${JBOSS_HOME}/standalone/configuration/standalone.xml \
	    -xsl:xsl/unpostgres.xsl \
	    -o:${JBOSS_HOME}/standalone/configuration/standalone.xml \
	    datasource=SpadeDS

if [ -f ${JBOSS_HOME}/standalone/deployments/spade.war ] ; then
    echo "Undeploying existing spade.war"
    rm -f ${JBOSS_HOME}/standalone/deployments/spade.war
fi
