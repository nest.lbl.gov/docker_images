<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:datasources:5.0" xmlns="urn:jboss:domain:datasources:5.0" version="2.0">
  <!-- parameters set when XSL is invoked -->
  <xsl:param name="datasource">ServiceDS</xsl:param>

  <!-- declare newline variable -->
  <xsl:variable name="newline">
    <xsl:text>
</xsl:text>

  </xsl:variable>
  <!-- copy all parts that are not explicitly changed -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="jboss:datasources/jboss:datasource">
    <xsl:choose>

      <!-- remove "datasource" if it already exists -->
      <xsl:when test="@jndi-name=concat('java:jboss/datasources/', $datasource)">
</xsl:when>

      <!-- copy all other "datasource" elements  -->
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
