<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jboss="urn:jboss:domain:datasources:5.0" xmlns="urn:jboss:domain:datasources:5.0" version="2.0">
  <!-- parameters set when XSL is invoked -->
  <xsl:param name="user_name">service</xsl:param>
  <xsl:param name="password">service_pw</xsl:param>
  <xsl:param name="database">service</xsl:param>
  <xsl:param name="datasource">ServiceDS</xsl:param>
  <xsl:param name="postgres_jdbc_version">42.2.5</xsl:param>
  <xsl:param name="postgres_host">db</xsl:param>

  <!-- declare newline variable -->
  <xsl:variable name="newline">
    <xsl:text>
</xsl:text>

  </xsl:variable>
  <!-- copy all parts that are not explicitly changed -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="jboss:datasources/jboss:datasource">
    <xsl:choose>

      <!-- add "datasource" element after ExampleDS "datasource" element -->
      <xsl:when test="@jndi-name='java:jboss/datasources/ExampleDS'">
        <xsl:copy-of select="."/>
        <xsl:value-of select="$newline"/>
        <xsl:element name="datasource">
          <xsl:attribute name="jta">true</xsl:attribute>
          <xsl:attribute name="jndi-name">java:jboss/datasources/<xsl:value-of select="$datasource"/></xsl:attribute>
          <xsl:attribute name="pool-name">java:jboss/datasources/<xsl:value-of select="$datasource"/>_Pool</xsl:attribute>
          <xsl:attribute name="enabled">true</xsl:attribute>
          <xsl:attribute name="use-java-context">true</xsl:attribute>
          <xsl:element name="connection-url">jdbc:postgresql://<xsl:value-of select="$postgres_host"/>:5432/<xsl:value-of select="$user_name"/>?charSet=UTF8</xsl:element>
          <xsl:element name="driver">postgresql-<xsl:value-of select="$postgres_jdbc_version"/>.jar</xsl:element>
          <xsl:element name="pool">
            <xsl:element name="min-pool-size">50</xsl:element>
            <xsl:element name="max-pool-size">500</xsl:element>
          </xsl:element>
          <xsl:element name="security">
            <xsl:element name="user-name">
              <xsl:value-of select="$user_name"/>
            </xsl:element>
            <xsl:element name="password">
              <xsl:value-of select="$password"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="validation">
            <xsl:element name="valid-connection-checker">
              <xsl:attribute name="class-name">org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker</xsl:attribute>
            </xsl:element>
            <xsl:element name="validate-on-match">true</xsl:element>
            <xsl:element name="background-validation">false</xsl:element>
            <xsl:element name="exception-sorter">
              <xsl:attribute name="class-name">org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter</xsl:attribute>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:when>

      <!-- remove "datasource" if it already exists -->
      <xsl:when test="@jndi-name=concat('java:jboss/datasources/', $datasource)">
</xsl:when>

      <!-- copy all other "datasource" elements  -->
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
