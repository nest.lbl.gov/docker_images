#!/bin/bash
# Modified wildfly to deploy the SPADE Service

echo "Adding SpadeDS to wildfly configuration if not already present"

java -jar /usr/share/java/saxon.jar \
        -s:${JBOSS_HOME}/standalone/configuration/standalone.xml \
	    -xsl:xsl/postgres.xsl \
	    -o:${JBOSS_HOME}/standalone/configuration/standalone.xml \
	    user_name=${SPADE_USER} \
	    password=${SPADE_PASSWORD} \
	    database=${SPADE_USER} \
	    datasource=SpadeDS \
	    postgres_jdbc_version=${POSTGRES_JDBC_VERSION} \
	    postgres_host=${POSTGRES_HOST}

echo "Deploying spade-${SPADE_VERSION}.war as spade.war"

cp ${USER_HOME}/wars/spade-${SPADE_VERSION}.war \
    ${JBOSS_HOME}/standalone/deployments/spade.war
