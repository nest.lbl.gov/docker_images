# Updating the Nest SPADE image #

The following commands can be used to create and deliver a new [lblnest/spade](https://cloud.docker.com/u/lblnest/repository/docker/lblnest/spade) image to [Docker Hub](https://hub.docker.com/).

_Note:_ Change `SPADE\_TAG` and `NEST\_RELEASE\_TAG` to be their appropriate values.

    SPADE_TAG=4.5.0
    NEST_RELEASE_TAG="" # e.g. ="_1"
    git clone git@lz-git.ua.edu:lz_services/spade.git
    cd spade
    cd ${SPADE_TAG}
    git checkout ${SPADE_TAG}${NEST_RELEASE_TAG}
    docker build -t spade .
    docker tag spade lblnest/spade:${SPADE_TAG}${NEST_RELEASE_TAG}
    docker tag spade lblnest/spade:latest
    docker login

    docker push lblnest/spade:${SPADE_TAG}${NEST_RELEASE_TAG}
    docker push lblnest/spade:latest
