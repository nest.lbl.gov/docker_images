#
# This file attempts to be the moral equivalent of the Dockerfile, expect
#   it is run to install Wildfly in a running system and installing everything
#   in the user's area.
#

if [ "X" == "X${DOCKER_IMAGE_CLONE}" ] ; then
    echo "The path to the docker_image project must be specified in the DOCKER_IMAGE_CLONE variable" >&2
    exit 1
fi

WILDFLY_VERSION=19.0.0.Final
WILDFLY_SHA1=0d47c0e8054353f3e2749c11214eab5bc7d78a14
USER_HOME=${HOME}
JBOSS_HOME=${HOME}/wildfly
POSTGRES_JDBC_VERSION=42.2.12
MYSQL_CONNECTOR_VERSION=8.0.20

cd ${HOME}
curl  --insecure -s -O https://download.jboss.org/wildfly/${WILDFLY_VERSION}/wildfly-${WILDFLY_VERSION}.tar.gz
# On mac OS X us `shasum`
sha1sum wildfly-${WILDFLY_VERSION}.tar.gz | grep ${WILDFLY_SHA1}
tar zxvf wildfly-${WILDFLY_VERSION}.tar.gz
mv ${HOME}/wildfly-${WILDFLY_VERSION} ${JBOSS_HOME}
rm wildfly-${WILDFLY_VERSION}.tar.gz

curl  --insecure -s -L -O https://jdbc.postgresql.org/download/postgresql-${POSTGRES_JDBC_VERSION}.jar
mv postgresql-${POSTGRES_JDBC_VERSION}.jar ${JBOSS_HOME}/standalone/deployments/
curl --insecure -s -L -O https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz
tar zxvf mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz \
    mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar
mv mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar \
    ${JBOSS_HOME}/standalone/deployments/
rmdir mysql-connector-java-${MYSQL_CONNECTOR_VERSION}
rm -f mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz

(cd ${DOCKER_IMAGE_CLONE}/wildfly/${WILDFLY_VERSION} ; cp patch_standalone.sh patch_wildfly-init-redhat.sh patch_wildfly.conf ${JBOSS_HOME}/)
patch ${JBOSS_HOME}/bin/standalone.sh ${JBOSS_HOME}/patch_standalone.sh
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly-init-redhat.sh  ${JBOSS_HOME}/patch_wildfly-init-redhat.sh
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly.conf ${JBOSS_HOME}/patch_wildfly.conf
(cd ${JBOSS_HOME} ; rm patch_standalone.sh patch_wildfly-init-redhat.sh patch_wildfly.conf)

# Check the install by running
#    ~/wildfly/bin/standalone.sh --debug
