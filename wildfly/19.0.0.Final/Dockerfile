#
# Derived from https://hub.docker.com/r/jboss/wildfly/dockerfile

# Use latest jboss/base-jdk:11 image as the base
FROM jboss/base-jdk:11

USER root

# Make ssh/scp available
RUN yum install -y openssh-clients.x86_64 patch.x86_64 sudo

# Set up execution of user mapping command
RUN set -x \
    && mkdir -p /etc/sudoers.d \
    && echo "jboss ALL=(root) NOPASSWD: /map_user" > /etc/sudoers.d/jboss \
    && chmod 440 /etc/sudoers.d/jboss \
    && echo "ghost ALL=(jboss) NOPASSWD: ALL" > /etc/sudoers.d/ghost \
    && chmod 440 /etc/sudoers.d/ghost \
    && chmod 777 /opt
COPY docker-entrypoint.sh /usr/local/bin/
COPY ./map_user ./deploy_wildfly ./populate_standalone_workdir /

USER jboss

# Set the wildfly/jboss, LUX-Zeplin services and launch env variables in one line to minimize layer count
# LAUNCH_JBOSS_IN_BACKGROUND ensures signals are forwarded to the JVM process correctly for graceful shutdown
#
# _NOTE_: The `extras` directory can not be in the `wildfly` tree directory because of the way `map_user` runs.
ENV WILDFLY_VERSION=19.0.0.Final \
    WILDFLY_SHA1=0d47c0e8054353f3e2749c11214eab5bc7d78a14 \
    USER_HOME=/opt/jboss \
    JBOSS_HOME=/opt/wildfly \
    STANDALONE_WORKDIR=/opt/wildfly/workdir/standalone \
    JBOSS_EXTRAS=/opt/wildfly/workdir/extras \
    POSTGRES_JDBC_VERSION=42.2.12 \
    MYSQL_CONNECTOR_VERSION=8.0.20 \
    LAUNCH_JBOSS_IN_BACKGROUND=true

# Add the WildFly distribution to /opt. Jboss will be the owner as it is the user while being unpacked.
# Make sure the distribution is available from a well-known place
RUN cd $HOME \
    && curl -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
    && sha1sum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1 \
    && tar xf wildfly-$WILDFLY_VERSION.tar.gz \
    && mv $HOME/wildfly-$WILDFLY_VERSION $JBOSS_HOME \
    && rm wildfly-$WILDFLY_VERSION.tar.gz

USER root
run chmod 755 /opt

USER jboss
# Add general services that may needed by any LUX-Zeplin JEE Application
RUN curl -s -L -O https://jdbc.postgresql.org/download/postgresql-${POSTGRES_JDBC_VERSION}.jar \
    && mv postgresql-${POSTGRES_JDBC_VERSION}.jar ${JBOSS_HOME}/standalone/deployments/ \
    && curl -s -L -O https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz \
    && tar zxvf mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz \
        mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar \
    && mv mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar \
        ${JBOSS_HOME}/standalone/deployments/ \
    && rmdir mysql-connector-java-${MYSQL_CONNECTOR_VERSION} \
    && rm -f mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz \
    && chown jboss ${JBOSS_HOME}/standalone/deployments/*

# Add is hook to allow local environment to be set up
COPY --chown=jboss:0 patch_standalone.sh patch_standalone.conf ${JBOSS_HOME}/
RUN patch ${JBOSS_HOME}/bin/standalone.sh ${JBOSS_HOME}/patch_standalone.sh \
    && patch ${JBOSS_HOME}/bin/standalone.conf ${JBOSS_HOME}/patch_standalone.conf \
    && rm ${JBOSS_HOME}/patch_standalone.sh ${JBOSS_HOME}/patch_standalone.conf

# Modify wildfly/standalone to execute in a read-only environment
#   Note: the file whose read privilege is set are just templates and the "real"
#         version of these files will be in the `${STANDALONE_WORKDIR}/configuration`
#         directory with the correct privileges.
RUN mkdir -p ${JBOSS_EXTRAS} \
    && ln -s ${JBOSS_EXTRAS} ${JBOSS_HOME}/ \
    && mkdir -p ${STANDALONE_WORKDIR} \
    && chmod 777 ${STANDALONE_WORKDIR} \
    && mv ${JBOSS_HOME}/standalone/configuration ${JBOSS_HOME}/standalone/configuration.readonly \
    && mv ${JBOSS_HOME}/standalone/deployments ${JBOSS_HOME}/standalone/deployments.readonly \
    && chmod go+r ${JBOSS_HOME}/standalone/configuration.readonly/application-roles.properties \
                  ${JBOSS_HOME}/standalone/configuration.readonly/application-users.properties \
                  ${JBOSS_HOME}/standalone/configuration.readonly/mgmt-groups.properties \
                  ${JBOSS_HOME}/standalone/configuration.readonly/mgmt-users.properties \
    && rm -fr ${JBOSS_HOME}/standalone/tmp \
    && ln -s ${STANDALONE_WORKDIR}/configuration ${JBOSS_HOME}/standalone/ \
    && ln -s ${STANDALONE_WORKDIR}/data ${JBOSS_HOME}/standalone/ \
    && ln -s ${STANDALONE_WORKDIR}/deployments ${JBOSS_HOME}/standalone/ \
    && ln -s ${STANDALONE_WORKDIR}/log ${JBOSS_HOME}/standalone/ \
    && ln -s ${STANDALONE_WORKDIR}/tmp ${JBOSS_HOME}/standalone/
VOLUME [ "/opt/wildfly/workdir/standalone", \
         "/opt/wildfly/workdir/extras" ]

# Expose the ports we're interested in
EXPOSE 8080

# Set the default command to run on boot
# This will boot WildFly in the standalone mode and bind to all interface
ENTRYPOINT ["docker-entrypoint.sh"]
