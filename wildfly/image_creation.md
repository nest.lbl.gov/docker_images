# Updating the Nest Wildfly image #

The following commands can be used to create and deliver a new [lblnest/wildfly](https://cloud.docker.com/u/lblnest/repository/docker/lblnest/wildfly) image to [Docker Hub](https://hub.docker.com/).

_Note:_ Change `WILDFLY\_TAG` and `NEST\_RELEASE\_TAG` to be their appropriate values.

    WILDFLY_TAG=19.0.0.Final
    NEST_RELEASE_TAG="" # e.g. ="_1"
    git clone git@gitlab.com:nest.lbl.gov/docker_images.git
    cd docker_images/wildfly
    cd ${WILDFLY_TAG}
    git checkout ${WILDFLY_TAG}${NEST_RELEASE_TAG}
    docker build -t wildfly .
    docker tag wildfly lblnest/wildfly:${WILDFLY_TAG}${NEST_RELEASE_TAG}
    docker tag wildfly lblnest/wildfly:latest
    docker login

    docker push lblnest/wildfly:${WILDFLY_TAG}${NEST_RELEASE_TAG}
    docker push lblnest/wildfly:latest
