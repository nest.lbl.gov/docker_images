[wildfly](https://gitlab.com/nest.lbl.gov/docker_images/wildfly) Contains the files and documentation needed to manage the Nest deployment of the Wildfly Application Server.

More detailed documentation can be found in the following documents:

*   Details about how to deploy an instance of this project are documented [here](deployment.md). This should be used to test each image after it is created to ensure it behaves correctly.

*   Building a new Docker image of this project is documented [here](image_creation.md).

# Versions #

Docker images, which can be found on [Docker Hub](https://hub.docker.com/r/lblnest/wildfly/) of the following versions have been created.

*   19.0.0.Final

    Built to use Wildfly 16.0.0.Final with the addition of:
    
    -    Postgres JDBC 42.2.12
    -    MySQL Connector J 8.0.20

*   16.0.0.Final

    Built to use Wildfly 16.0.0.Final with the addition of:
    
    -    Postgres JDBC 42.2.5
    -    MySQL Connector J 5.1.47
    -    Mongo Java Driver 3.10.1
