# Nest Java Enterprise Edition Service #

A number of services used by Nest run as [Java Enterprise
Edition](http://www.oracle.com/technetwork/java/javaee/overview/index.html)
applications. At present these applications are hosted by an instance of
[Wildfly Application Server](http://wildfly.org/) running on
nest.lbl.gov.

This document lays out the deployment of the Wildfly application server

The Wildfly instance is deployed in its own
[Docker](https://www.docker.com/) container, with links to the necessary
DBMS containers. The server's container itself is created using a
[Dockerfile](https://gitlab.com/nest.lbl.gov/docker_images/wildfly/blob/master/19.0.0.Final/Dockerfile)
created for Nest, based off the official [Wildfly
Dockerfile](https://github.com/jboss-dockerfiles/wildfly/blob/master/Dockerfile).

## Creating the Wildfly environment ##

### Docker User-Defined Network ###

The Wildfly application server for Nest relies on a number of
DBMSs containers, this means that it needs to be able to connect to
those container and it does this over a docker [user-defined
network](https://docs.docker.com/engine/userguide/networking/#user-defined-networks). Therefore,
before creating the necessary Docker container we need to create the
appropriate network using the following command.

    docker network create --driver bridge nest_network


### Creating the Wildfly container ###

The following environmental variables will be used to create the
wildfly container, so need to be declared first.

    WILDFLY_HOST_STANDALONE=${HOME}/docker/jboss/workdir/standalone
    WILDFLY_HOST_EXTRAS=${HOME}/docker/jboss/extras
    ENV_DIR=${HOME}/docker/env/

The Wildfly container is created from the [officially released
image](https://hub.docker.com/r/jboss/wildfly/) that is connected to
appropriate DBMS containers. In order to make the host user match the
image user, the following needs to be executed before the container is
run.

    HOST_UID=$(id -u)
    HOST_GID=$(id -g)
    mkdir -p ${ENV_DIR}
    cat > ${ENV_DIR}/wildfly.env << EOF
    HOST_UID=${HOST_UID}
    HOST_GID=${HOST_GID}
    EOF

Also, for versions from 13.0.0.Final.1 and beyond, the image is designed
as a read-only image so that is can easily be deployed in cloud-like
scenarios. Therefore work area neeed to be created on the host using the
following:

    mkdir -p ${WILDFLY_HOST_STANDALONE} \
        ${WILDFLY_HOST_EXTRAS}
    

Once that has been executed a `wildfly` container can be instantiated
with the following command (Note: the `--volume` options can be dropped for
pre-13.0.0.Final.1 versions.).

    docker run -i -t -d --name nest_wildfly \
        --network nest_network \
        --publish 127.0.0.1:8080:8080 \
        --volume ${WILDFLY_HOST_STANDALONE}:/opt/wildfly/workdir/standalone \
        --volume ${WILDFLY_HOST_EXTRAS}:/opt/wildfly/workdir/extras \
        --env-file ${ENV_DIR}/wildfly.env \
        lblnest/wildfly

(Note: The middle number of the three in the `--publish` statement is
the port on the `localhost` through which the server can be contacted,
this may need to be changed to avoid confilcts with other deployments.)

## Running the Wildfly container ##

Under normal circumstances Wildfly container should be running along
with the appropriate DBMS containers. This can be checked by executing
the following command.

    docker container ls --all

If the status of `nest_wildfly` does not read something along the lines of
`Up n minutes`, then action need to be taken. If the status is something
like `Up n minutes (Paused)`, then the container has been paused and
need to be resumed with the following command (where `<name>` is
replaced by the name of the container that is being resumed).

    docker container unpause <name>

When a container has been stopped its status will read something similar
to `Exited (0) n minutes ago` and in this case is needs to be started
with the following command.

    docker container start <name>

If the Wildfly container (and appropriate DBMS containers) is not listed
as a result of the `container ls --all` command then it need to be
re-created and modified as explained above. If the appropriate DBMS
containers do not exist then they need to be recovered. The details of
how to do that are beyond the scope of the document.

## Inspecting the `wildfly` Container ##

The following command can be used to access the container running
`wildfly` and confirm it is deployed as expected.

    docker exec -it nest_wildfly /bin/bash


## Cleaning up ##

To clean up all of the artifacts used in this deployment, you can run the following.

    docker container rm -f nest_wildfly
    docker image rm lblnest/wildfly:latest
    docker network rm nest_network
