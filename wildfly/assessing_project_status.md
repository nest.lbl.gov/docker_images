# Assessing the project status #

After a while away from a project it is easy to forget the status of it when returning to work with it. This document is a quick cheat-sheet on how to check the current status.

1.  Go to the [wildfly repository](.) and see which is the latest version to have it own directory.

2.  Check the list of tags to find out the latest tag for this version.

    If there is no tag for the latest version, repeat for the proceeding version.

3.  Check that this tag is deployed on [Docker Hub](https://cloud.docker.com/u/lblnest/repository/docker/lblnest/docker-images/tags)

4.  If not, then [build and post](image_creation.md) the image of the latest tag to Docker Hub.
