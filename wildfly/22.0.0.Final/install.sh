#!/bin/bash
#
# This file attempts to be the moral equivalent of the Dockerfile, expect
#   it is run to install Wildfly in a running system and installing everything
#   in the user's area.
#

if [ "X" == "X${DOCKER_IMAGE_CLONE}" ] ; then
    echo "The path to the docker_images project must be specified in the DOCKER_IMAGE_CLONE variable" >&2
    exit 1
fi

# Note: POSTGRES_JDBC_VERSION > 42.2.14 failed with wildfly 22.0.0.Final
WILDFLY_VERSION=22.0.0.Final
WILDFLY_SHA1=24b002c245171f56276675f939bc7b0413fccf40
JBOSS_HOME=${HOME}/wildfly
USER_HOME=${HOME}
STANDALONE_WORKDIR=${JBOSS_HOME}/workdir/standalone
JBOSS_EXTRAS=${JBOSS_HOME}/workdir/extras
POSTGRES_JDBC_VERSION=42.2.14
MYSQL_CONNECTOR_VERSION=8.0.23


cd ${HOME}
curl --insecure -s -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz
case "`uname`" in
    Darwin*)
        shasum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1
        result=${?}
        ;;

    *)
        sha1sum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1
        result=${?}
        ;;

esac
if [ 0 != ${result} ] ; then
    exit 2
fi
tar -xf wildfly-$WILDFLY_VERSION.tar.gz
mv $HOME/wildfly-$WILDFLY_VERSION $JBOSS_HOME
rm wildfly-$WILDFLY_VERSION.tar.gz

curl --insecure -s -L -O https://jdbc.postgresql.org/download/postgresql-${POSTGRES_JDBC_VERSION}.jar
mv postgresql-${POSTGRES_JDBC_VERSION}.jar ${JBOSS_HOME}/standalone/deployments/
curl --insecure -s -L -O https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz
tar zxvf mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz \
    mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar
mv mysql-connector-java-${MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar \
    ${JBOSS_HOME}/standalone/deployments/

# This section adds in the MDT time zone
jar xf ${JBOSS_HOME}/standalone/deployments/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar com/mysql/cj/util/TimeZoneMapping.properties
cat >> com/mysql/cj/util/TimeZoneMapping.properties << EOF
MDT=America/Denver
EOF
jar uf ${JBOSS_HOME}/standalone/deployments/mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.jar com/mysql/cj/util/TimeZoneMapping.properties
rm com/mysql/cj/util/TimeZoneMapping.properties
rmdir -p com/mysql/cj/util
# End

rmdir mysql-connector-java-${MYSQL_CONNECTOR_VERSION}
rm -f mysql-connector-java-${MYSQL_CONNECTOR_VERSION}.tar.gz

(cd ${DOCKER_IMAGE_CLONE}/wildfly/${WILDFLY_VERSION} ; cp patch_standalone.sh patch_standalone.conf patch_wildfly-init-redhat.sh patch_wildfly.conf ${JBOSS_HOME}/)
patch ${JBOSS_HOME}/bin/standalone.sh ${JBOSS_HOME}/patch_standalone.sh
patch ${JBOSS_HOME}/bin/standalone.conf ${JBOSS_HOME}/patch_standalone.conf
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly-init-redhat.sh  ${JBOSS_HOME}/patch_wildfly-init-redhat.sh
patch ${JBOSS_HOME}/docs/contrib/scripts/init.d/wildfly.conf ${JBOSS_HOME}/patch_wildfly.conf
(cd ${JBOSS_HOME} ; rm patch_standalone.sh patch_standalone.conf patch_wildfly-init-redhat.sh patch_wildfly.conf)

# Check the install by running
#    ~/wildfly/bin/standalone.sh --debug
