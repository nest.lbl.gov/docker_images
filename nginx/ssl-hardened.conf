#
# Test against https://SSLLabs.com/ssltest
#

load_module /etc/nginx/modules/ngx_http_shibboleth_module.so;
load_module /etc/nginx/modules/ngx_http_headers_more_filter_module.so;

# Not needed when running in container, as launched
#   as the corret user.
#user  nginx;
worker_processes  1;
pid /var/cache/nginx/nginx.pid;

error_log  /var/log/nginx/error.log warn;

events {
    worker_connections  4096;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    # supress version and OS identification
    server_tokens off;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    # SSL hardening, Based https://github.com/llambiel/letsecureme/blob/master/etc/nginx/sites-enabled/default.conf
    #                  and https://geekflare.com/nginx-webserver-security-hardening-guide/
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA26:ECDHE-ECDSA-AES256-SHA384';
    ssl_prefer_server_ciphers On;
    ssl_session_cache shared:SSL:128m;
    add_header Strict-Transport-Security "max-age=31557600; includeSubDomains";
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 131.243.5.1;

    # Avoid Clickjacking Attack
    add_header Referrer-Policy origin-when-cross-origin;
    add_header X-Content-Type-Options "nosniff" always;
    add_header X-Frame-Options "SAMEORIGIN" always;

    # Mitigate Cross-Site scripting attack
    add_header X-Xss-Protection "1; mode=block";

    include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/sites-enabled/*;
}
