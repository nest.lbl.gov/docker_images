# Updating the Nest `nginx` image #

The following commands can be used to create and deliver a new `lblnest/nginx` image to [Docker Hub](https://hub.docker.com/).

_Note:_ Change `NEST\_NGINX\_TAG` and `NEST\_RELEASE\_TAG` to be their appropriate values.

    NGINX_VERS=1.18.0
    NEST_RELEASE_TAG="" # e.g. ="_1"
    NEST_NGINX_TAG=${NGINX_VERS}${NEST_NGINX_TAG}
    git clone git@gitlab.com:nest.lbl.gov/docker_images.git
    cd docker_images/nginx
    git checkout NEST_NGINX_TAG
    docker build -t nest-nginx .

To test the resulting image run the following:

    openssl req -x509 \
        -subj '/CN=localhost' \
        -newkey rsa:4096 \
        -keyout test/ssl/private/key.pem \
        -out test/ssl/certs/cert.pem \
        -days 365 \
        -nodes
    docker run --name nest-nginx -d \
        -v $(pwd)/test/conf.d:/etc/nginx/conf.d:ro \
        -v $(pwd)/test/ssl:/etc/nginx/ssl:ro \
        -v $(pwd)/test/html:/usr/share/nginx/html:ro \
        -v $(pwd)/test/secure:/usr/share/nginx/secure:ro \
        -p 8080:8080 \
        -p 8443:8443 \
        nest-nginx
    wget -O tmp.html http://localhost:8080/
    diff tmp.html test/html/index.html
    wget -O tmp.html --ca-certificate=test/ssl/certs/cert.pem https://localhost:8443/
    diff tmp.html test/secure/index.html
    rm tmp.html

If it works successfully in can now be pushed to the Docker Hub.

    docker tag nest-nginx lblnest/nginx:${NEST_NGINX_TAG}
    docker tag nest-nginx lblnest/nginx:latest
    docker login

    docker push lblnest/nginx:${NEST_NGINX_TAG}
    docker push lblnest/nginx:latest
`